import numpy as np
import sympy as sm
import sympy.physics.mechanics as me
from contextlib import redirect_stdout


# import pickle
# from contextlib import redirect_stdout
from sympy.printing import latex as spl


class Manipulator(object):

    ''' Planar Manipulator '''

    def __init__(self, n_joints=2):

        self.n = n_joints
        self.q = me.dynamicsymbols('q:{}'.format(self.n)) # generalised coordinates
        self.qd = me.dynamicsymbols('qd:{}'.format(self.n))
        self.qdd = me.dynamicsymbols('qdd:{}'.format(self.n))

        self.px = me.dynamicsymbols('px:{}'.format(self.n))
        self.py = me.dynamicsymbols('py:{}'.format(self.n))
        # self.p = me.dynamicsymbols('p:{}'.format(self.n))

        self.plx = me.dynamicsymbols('plx:{}'.format(self.n))
        self.ply = me.dynamicsymbols('ply:{}'.format(self.n))
        # self.pl = me.dynamicsymbols('pl:{}'.format(self.n))

        self.pl = sm.zeros(3,self.n)     # center of gravity pos
        self.p = sm.zeros(3,self.n+1)

        self.a = sm.symbols('a:{}'.format(self.n))    # link length
        self.l = sm.symbols('l:{}'.format(self.n))    # link center of gravity
        self.m = sm.symbols('m:{}'.format(self.n))
        self.I = sm.symbols('I:{}'.format(self.n))
        self.g = sm.symbols('g')
        self.g0 = sm.Matrix([0, -self.g, 0])

        self.z = sm.zeros(3,self.n)

        self.T_ = me.dynamicsymbols('Tao:{}'.format(self.n))

        self.setBot()


    def setBot(self):

        q_ = 0
        px_ = 0
        py_ = 0

        plx_ = 0
        ply_ = 0

        self.m_ = list()
        self.I_ = list()

        self.Q = sm.zeros(self.n,1)
        self.Qd = sm.zeros(self.n,1)
        self.Qdd = sm.zeros(self.n,1)

        self.Tao = sm.zeros(self.n,1)

        for i in range(self.n):

            self.z[2,i] = 1

            q_ += self.q[i]

            self.plx[i] = self.l[i]*sm.cos(q_) + px_
            self.ply[i] = self.l[i]*sm.sin(q_) + py_
            self.pl[:,i] = [self.plx[i], self.ply[i],0]

            self.px[i] = self.a[i]*sm.cos(q_) + px_
            self.py[i] = self.a[i]*sm.sin(q_) + py_
            self.p[:,i+1] = [self.px[i], self.py[i], 0]

            px_ += self.px[i]
            py_ += self.py[i]

            self.m_.append(self.m[i])
            Itemp = sm.zeros(3,3)
            Itemp[2,2] = self.I[i]
            self.I_.append(Itemp)

            self.Q[i,0] = self.q[i]
            self.Qd[i,0] = self.qd[i]
            self.Qdd[i,0] = self.qdd[i]

            self.Tao[i] = self.T_[i]


    def computeJacobian(self):

        ''' compute geometric Jacobian '''

        self.Jg = sm.zeros(6,self.n)
        self.Jg_ = list()

        for i in range(self.n):
            for j in range(i+1):

                # position
                P_j_li = self.z[:,j].cross(self.pl[:,i] - self.p[:,j])
                # orient
                O_j_li = self.z[:,j]

                self.Jg[0:3,j] = P_j_li
                self.Jg[3:,j] = O_j_li

            self.Jg_.append(self.Jg)
            self.Jg = sm.zeros(6,self.n)


    def computeInertiaMatrix(self):

        self.B = sm.zeros(self.n, self.n)
        for i in range(self.n):

            self.B += self.m_[i] * sm.transpose(self.Jg_[i][0:3,:]) * (self.Jg_[i][0:3,:])  +  sm.transpose(self.Jg_[i][3:6,:]) * self.I_[i] * (self.Jg_[i][3:6,:])


    def computeChristoffelMatrix(self):
        self.C = sm.zeros(self.n,self.n)
        Ctemp = sm.zeros(self.n,self.n)
        C_ = list()

        for i in range(self.n):
            for j in range(self.n):
                for k in range(self.n):
                    Ctemp[j,k] = 0.5 * ( sm.diff(self.B[i,j], self.Q[k]) + sm.diff(self.B[i,k], self.Q[j]) - sm.diff(self.B[j,k], self.Q[i]) )
            C_.append(Ctemp)
            Ctemp = sm.zeros(self.n,self.n)

        for i in range(self.n):
            for j in range(self.n):
                for k in range(self.n):
                    self.C[i,j] += C_[i][j,k] * self.Qd[k,0]

    def computeGravityMatrix(self):

        self.G = sm.zeros(self.n,1)
        Gtemp = sm.zeros(1,1)
        G_ = list()

        for i in range(self.n):
            for j in range(self.n):
                 Gtemp =  - self.m_[j] * sm.transpose(self.g0) * (self.Jg_[j][0:3,i])
                 self.G[i,0] += Gtemp[0,0]


    def printMatrix(self, matrix, mode='inline'):

        ''' print in latex style '''

        print(spl(sm.simplify(matrix), mode=mode, mat_delim=""))

    def saveMatrix(self, matrix, path='./data.tex'):
        ''' save in a latex file'''

        d  = spl(sm.simplify(matrix),mode='inline')

        with open(path, 'w') as f:
            with redirect_stdout(f):
                sm.pprint(d, wrap_line=False)


    def motionEquation(self):

        # self.Equation = me.dynamicsymbols('Eq')

        self.Equation = self.B * self.Qdd + self.C * self.Qd + self.G - self.Tao
