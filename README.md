# robot_manipulator

This package provides symbolic dynamics equation of a planar robot manipulator.

## Environment

- python 3.5
- numpy
- sympy

![](planar2.png)