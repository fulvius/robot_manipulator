from manipulator import *

# create a manipulator robot with n_joints radial joints
bot = Manipulator(n_joints=2)

# compute geometric Jaobian
bot.computeJacobian()

# compute Inertia Matrix
bot.computeInertiaMatrix()

# compute christoffel symbols
bot.computeChristoffelMatrix()

# compute Gravity Matrix
bot.computeGravityMatrix()

# compute dynamic motion equation
bot.motionEquation()

# print 'matrix' in Latex style
bot.printMatrix(matrix=bot.Equation)

# save 'matrix' into a Latex file
bot.saveMatrix(matrix=bot.Equation)
